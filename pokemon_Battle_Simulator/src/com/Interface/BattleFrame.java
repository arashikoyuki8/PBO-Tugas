package com.Interface;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.User.*;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;
import java.awt.Color;

import javax.swing.SwingConstants;
import java.awt.Font;

@SuppressWarnings("serial")
public class BattleFrame extends JFrame {
	private Player vPlayer;
	private DifficultyAI vAI;
	private JPanel contentPane;
	private JLabel lbDecision;
	private JButton bSwitch, bFight, bBackFight, bBackSwitch;
	private JButton bMove1, bMove2, bMove3, bMove4;
	private JButton PartyPoke1, PartyPoke2, PartyPoke3, PartyPoke4, PartyPoke5, PartyPoke6;
	private JLabel PlayerPokeHP, OppPokeHP, OppPokeName, OpPokePos, lbTurn;
	private int vTurnCount;
	
	public BattleFrame(Player p1, DifficultyAI p2) {
		setTitle("Irwanto Danang Bahtiar");
		this.vPlayer = p1;
		this.vAI = p2;
		ImageIcon icons = new ImageIcon(this.getClass().getResource("/Icon2.png"));
		setIconImage(icons.getImage());
		initialize();
	}

	public void initialize(){
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 398);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton Surrend = new JButton("End Game");
		Surrend.setFont(new Font("Gill Sans MT Condensed", Font.BOLD, 18));
		Surrend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MainFrame.main(null);
			}
		});
		Surrend.setBounds(10, 11, 106, 25);
		contentPane.add(Surrend);
		
		//SwitchPokemon
		int vPokeIndex = vPlayer.mGetTeam().length;
		if (vPokeIndex != 0 && vPokeIndex >= 1) {
			PartyPoke1 = new JButton("New button");
			PartyPoke1.setBounds(21, 81, 121, 35);
			contentPane.add(PartyPoke1);
			PartyPoke1.setVisible(false);
		}
		if (vPokeIndex != 1 && vPokeIndex >= 2) {
			PartyPoke2 = new JButton("New button");
			PartyPoke2.setBounds(161, 81, 121, 35);
			contentPane.add(PartyPoke2);
			PartyPoke2.setVisible(false);
		}
		if (vPokeIndex != 2 && vPokeIndex >= 3) {
			PartyPoke3 = new JButton("New button");
			PartyPoke3.setBounds(21, 161, 121, 35);
			contentPane.add(PartyPoke3);
			PartyPoke3.setVisible(false);
		}
		if (vPokeIndex != 3 && vPokeIndex >= 4) {
			PartyPoke4 = new JButton("New button");
			PartyPoke4.setBounds(161, 161, 121, 35);
			contentPane.add(PartyPoke4);
			PartyPoke4.setVisible(false);
		}
		if (vPokeIndex != 4 && vPokeIndex >= 5) {
			PartyPoke5 = new JButton("New button");
			PartyPoke5.setBounds(21, 240, 121, 35);
			contentPane.add(PartyPoke5);
			PartyPoke5.setVisible(false);
		}
		if (vPokeIndex != 5 && vPokeIndex >= 6) {
			PartyPoke6 = new JButton("New button");
			PartyPoke6.setBounds(161, 240, 121, 35);
			contentPane.add(PartyPoke6);
			PartyPoke6.setVisible(false);
		}
		
		bBackSwitch = new JButton("BACK");
		bBackSwitch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bBackSwitch.setVisible(false);
				lbDecision.setVisible(true);
				bSwitch.setVisible(true);
				bFight.setVisible(true);
				PartyPoke1.setVisible(false);
				PartyPoke2.setVisible(false);
				PartyPoke3.setVisible(false);
				PartyPoke4.setVisible(false);
				PartyPoke5.setVisible(false);
				PartyPoke6.setVisible(false);
			}
		});
		bBackSwitch.setBounds(544, 332, 89, 23);
		bBackSwitch.setVisible(false);
		contentPane.add(bBackSwitch);
		
		lbTurn = new JLabel("");
		updateTurn();
		lbTurn.setForeground(new Color(0, 255, 0));
		lbTurn.setFont(new Font("Microsoft JhengHei Light", Font.BOLD, 18));
		lbTurn.setHorizontalAlignment(SwingConstants.CENTER);
		lbTurn.setBounds(287, 11, 74, 25);
		contentPane.add(lbTurn);
		
		//Moves
		int moveIndex = vPlayer.getPokemon().getPartyMoves().size();
		
		if (moveIndex != 0 && moveIndex >= 1) {
			bMove1 = new JButton(vPlayer.getPokemon().getPartyMoves().get(0).getMoves());
			bMove1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					vAI.opponentDecision(vPlayer.getPokemon());
					vPlayer.guiChooseMove(0);
					if (vPlayer.getPokemon().getTSPE() >= vAI.getPokemon().getTSPE()) {
						vPlayer.guiPlayerAction(vAI.getPokemon());
						updateOpponentHP();
						vAI.mOpponentAction();
						updatePlayerHP();
					} else if (vPlayer.getPokemon().getTSPE() < vAI.getPokemon().getTSPE()) {
						vAI.mOpponentAction();
						updatePlayerHP();
						vPlayer.guiPlayerAction(vAI.getPokemon());
						updateOpponentHP();
					}
					updateTurn();
					if (moveIndex != 0 && moveIndex >= 1) {
						bMove1.setVisible(false);
					}
					bBackFight.setVisible(false);
					lbDecision.setVisible(true);
					bSwitch.setVisible(true);
					bFight.setVisible(true);
					if (moveIndex != 1 && moveIndex >= 2) {
						bMove2.setVisible(false);
					}
					if (moveIndex != 2 && moveIndex >= 3) {
						bMove3.setVisible(false);
					}
					if (moveIndex != 3 && moveIndex >= 4) {
						bMove4.setVisible(false);
					}
				}
			});
			bMove1.setBounds(104, 278, 123, 33);
			contentPane.add(bMove1);
			bMove1.setVisible(false);
		}
		if (moveIndex != 1 && moveIndex >= 2) {
			bMove2 = new JButton(vPlayer.getPokemon().getPartyMoves().get(1).getMoves());
			bMove2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				}
			});
			bMove2.setBounds(373, 278, 123, 33);
			contentPane.add(bMove2);
			bMove2.setVisible(false);
		}
		if (moveIndex != 2 && moveIndex >= 3) {
			bMove3 = new JButton(vPlayer.getPokemon().getPartyMoves().get(2).getMoves());
			bMove3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				}
			});
			bMove3.setBounds(104, 317, 123, 33);
			contentPane.add(bMove3);
			bMove3.setVisible(false);
		}
		if (moveIndex != 3 && moveIndex >= 4) {
			bMove4 = new JButton(vPlayer.getPokemon().getPartyMoves().get(3).getMoves());
			bMove4.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				}
			});
			bMove4.setBounds(373, 322, 123, 33);
			contentPane.add(bMove4);
			bMove4.setVisible(false);
		}
		
		bBackFight = new JButton("BACK");
		bBackFight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bBackFight.setVisible(false);
				lbDecision.setVisible(true);
				bSwitch.setVisible(true);
				bFight.setVisible(true);
				bMove1.setVisible(false);
				bMove2.setVisible(false);
				bMove3.setVisible(false);
				bMove4.setVisible(false);
			}
		});
		bBackFight.setBounds(544, 332, 89, 23);
		bBackFight.setVisible(false);
		contentPane.add(bBackFight);
		
		//Decision
		this.lbDecision = new JLabel("What will "+vPlayer.getPokemon().getPartyName()+" do?");
		lbDecision.setBounds(10, 274, 623, 23);
		contentPane.add(lbDecision);
		lbDecision.setFont(new Font("Gill Sans MT Condensed", Font.ITALIC, 23));
		lbDecision.setHorizontalAlignment(SwingConstants.CENTER);
		
		this.bSwitch = new JButton("SWITCH");
		bSwitch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbDecision.setVisible(false);
				bSwitch.setVisible(false);
				bFight.setVisible(false);
				if (vPokeIndex != 0 && vPokeIndex >= 1) {
					PartyPoke1.setVisible(true);
				}
				if (vPokeIndex != 1 && vPokeIndex >=2) {
					PartyPoke2.setVisible(true);
				}
				if (vPokeIndex != 2 && vPokeIndex >=3) {
					PartyPoke3.setVisible(true);
				}
				if (vPokeIndex != 3 && vPokeIndex >= 4) {
					PartyPoke4.setVisible(true);
				}
				if (vPokeIndex != 4 && vPokeIndex >= 5) {
					PartyPoke5.setVisible(true);
				}
				if (vPokeIndex != 5 && vPokeIndex >= 6) {
					PartyPoke6.setVisible(true);
				}
				bBackSwitch.setVisible(true);
			}
		});
		bSwitch.setBounds(388, 308, 121, 35);
		contentPane.add(bSwitch);
		bSwitch.setFont(new Font("Gill Sans MT Condensed", Font.BOLD, 18));
			
		this.bFight = new JButton("FIGHT");
		bFight.setBounds(130, 308, 121, 35);
		contentPane.add(bFight);
		bFight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbDecision.setVisible(false);
				bSwitch.setVisible(false);
				bFight.setVisible(false);
				if (moveIndex != 0 && moveIndex >= 1) {
					bMove1.setVisible(true);
				}
				if (moveIndex != 1 && moveIndex >=2) {
					bMove2.setVisible(true);
				}
				if (moveIndex != 2 && moveIndex >=3) {
					bMove3.setVisible(true);
				}
				if (moveIndex != 3 && moveIndex >= 4) {
					bMove4.setVisible(true);
				}
				bBackFight.setVisible(true);
			}
		});
		bFight.setFont(new Font("Gill Sans MT Condensed", Font.BOLD, 18));
		
		//Position
		OpPokePos = new JLabel();
		updateOpponentSprite();
		OpPokePos.setVerticalAlignment(SwingConstants.BOTTOM);
		OpPokePos.setBounds(362, 24, 163, 153);
		contentPane.add(OpPokePos);
		
		OppPokeName = new JLabel("");
		updateOpponentName();
		OppPokeName.setHorizontalAlignment(SwingConstants.CENTER);
		OppPokeName.setForeground(new Color(0, 255, 0));
		OppPokeName.setFont(new Font("Gill Sans MT Condensed", Font.ITALIC, 26));
		OppPokeName.setBounds(496, 42, 123, 25);
		contentPane.add(OppPokeName);
		
		OppPokeHP = new JLabel("");
		updateOpponentHP();
		OppPokeHP.setForeground(new Color(127, 255, 0));
		OppPokeHP.setHorizontalAlignment(SwingConstants.CENTER);
		OppPokeHP.setFont(new Font("Gill Sans MT Condensed", Font.BOLD, 17));
		OppPokeHP.setBounds(506, 78, 97, 25);
		contentPane.add(OppPokeHP);
		
		JLabel PlayerPokeName = new JLabel(vPlayer.getPokemon().getPartyName());
		PlayerPokeName.setFont(new Font("Gill Sans MT Condensed", Font.ITALIC, 26));
		PlayerPokeName.setHorizontalAlignment(SwingConstants.CENTER);
		PlayerPokeName.setForeground(new Color(0, 255, 0));
		PlayerPokeName.setBounds(35, 123, 123, 25);
		contentPane.add(PlayerPokeName);
		
		PlayerPokeHP = new JLabel("");
		updatePlayerHP();
		PlayerPokeHP.setForeground(new Color(127, 255, 0));
		PlayerPokeHP.setFont(new Font("Gill Sans MT Condensed", Font.BOLD, 16));
		PlayerPokeHP.setHorizontalAlignment(SwingConstants.CENTER);
		PlayerPokeHP.setBounds(45, 159, 97, 25);
		contentPane.add(PlayerPokeHP);
		
		URL vPlayerPokemon = this.getClass().getResource("/back-"+vPlayer.getPokemon().getPartyName()+".gif");
		ImageIcon PlayerPokemon = new ImageIcon(vPlayerPokemon);
		JLabel PlayerPokePos = new JLabel(PlayerPokemon);
		PlayerPokePos.setVerticalAlignment(SwingConstants.BOTTOM);
		PlayerPokePos.setHorizontalAlignment(SwingConstants.CENTER);
		PlayerPokePos.setBounds(119, 136, 163, 153);
		contentPane.add(PlayerPokePos);
		
		//Background
		JLabel BackgroundBattle = new JLabel("");
		Image battleBg = new ImageIcon(this.getClass().getResource("/battleBG.png")).getImage();
		BackgroundBattle.setIcon(new ImageIcon(battleBg));
		BackgroundBattle.setBounds(0, 0, 650, 366);
		contentPane.add(BackgroundBattle);
	}
	
	
	// Battle Method (set hp, update hp and such)
	private void updatePlayerHP() {
		PlayerPokeHP.setText(vPlayer.getPokemon().getRHP()+"/"+vPlayer.getPokemon().getMHP());
		if (vPlayer.getPokemon().getRHP() == 0) {
			vPlayer.mPlayerSwitch();
		}
	}
	private void updateOpponentHP() {
		OppPokeHP.setText(vAI.getPokemon().getRHP() + "/" + vAI.getPokemon().getMHP());
		if (vAI.getPokemon().getRHP() == 0) {
			vAI.opponentCPokemon();
			updateOpponentSprite();
			updateOpponentName();
			OppPokeHP.setText(vAI.getPokemon().getRHP() + "/" + vAI.getPokemon().getMHP());
		}
	}
	private void updateOpponentName() {
		OppPokeName.setText(vAI.getPokemon().getPartyName());
	}
	private void updateOpponentSprite() {
		URL vOpponentPokemon = this.getClass().getResource("/front-"+vAI.getPokemon().getPartyName()+".gif");
		ImageIcon OpponentPokemon = new ImageIcon(vOpponentPokemon);
		OpPokePos.setIcon(OpponentPokemon);
	}
	private void updateTurn() {
		this.vTurnCount += 1;
		lbTurn.setText(" "+ vTurnCount + " ");
	}
}
