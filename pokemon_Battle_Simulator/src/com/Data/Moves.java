package com.Data;

import java.util.*;
import java.io.*;
import java.net.URISyntaxException;

public class Moves {
	private String vMoveName;
	private int vMovePower;
	private Type vMoveType;
	private boolean isSpecial;
	
	public Moves(String pMoveName) {
		this.vMoveName = pMoveName;
	}
	public void setMovePower(int pMovePower) {
		this.vMovePower = pMovePower;
	}
	public void setMoveType(Type pMoveType) {
		this.vMoveType = pMoveType;
	}
	public void setIsSpecial(boolean pIsSpecial){
		this.isSpecial = pIsSpecial; 
	}
	public String getMoves() {
		return vMoveName;
	}
	public int getMovePower() {
		return vMovePower;
	}
	public Type getMoveType() {
		return vMoveType;
	}
	public boolean IsSpecial() {
		return isSpecial;
	}
	@Override
	public String toString() {
		return vMoveName;
	}
	
	public static List<Moves> readMoves (String filename) throws IOException, URISyntaxException {
		List<Moves> tMoveList = new ArrayList<>();
		
		try(BufferedReader br = new BufferedReader(new FileReader(filename))){
			String line;
			Moves move = null;
			DataReader dr = new DataReader();
			List<Type> tType = dr.typeReader();
			while ((line = br.readLine()) != null) {
				String[] parts = line.split(":");
				switch(parts[0]) {
				case "Name":
					if (move != null) {
						tMoveList.add(move);
					}
					move = new Moves(parts[1]);
					break;
				case "Power":
					int power = Integer.parseInt(parts[1]);
					move.setMovePower(power);
					break;
				case "Type":
					String type = parts[1];
					for (Type t : tType) {
						if (t.getType().equals(type)) {
							move.setMoveType(t);
							break;
						}
					}
					break;
				case "Category":
					boolean category = Boolean.parseBoolean(parts[1]);
					move.setIsSpecial(category);
					break;
					default:
						throw new IllegalArgumentException("invalid input: "+parts[0]);
				}
			}
			if (move != null) {
				tMoveList.add(move);
			}
		}
		
		return tMoveList;
	}
}
