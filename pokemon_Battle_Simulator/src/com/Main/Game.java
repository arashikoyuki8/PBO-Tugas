package com.Main;

import java.net.URISyntaxException;
import java.util.List;
import com.Data.*;
import com.User.*;

/********************************************************************
 * 				 POKEMON BATTLE SIMULATOR				 			*
 * Name			: Irwanto Danang Bahtiar				 			*
 * NIM			: 1217050070							 			*
 * Subject		: IF214005 - Object Oriented Programming (Practice) *
 * Term			: IV (Empat)							 			*
 * Instructor	: Muhammad Insan Al Amin MT				 			*
 ********************************************************************/
/**********************************************************
 *					     USE CASE    					  *
 * 1. Pokemon Battle 							 	 : 10 *
 * 2. List Pokemon									 : 9  *
 * 3. Item in Battle								 : 7  *
 * 4. Party Builder (Currently using .txt)		 	 : 9  *
 * 5. Ability in battle 						 	 : 7  *
 * 6. Field Effect									 : 7  *
 * 7. List Move										 : 9  *
 * 8. Type Advantage								 : 9  *
 **********************************************************/

public class Game {
	public static void main(String[] args) throws URISyntaxException {
		// TODO Auto-generated method stub
		int x = 0;
		System.out.println("Choose Difficulty:");
		System.out.println("1. Easy\n2. Normal\n3. Hard");
		
		DataReader B = new DataReader();
		B.setPlayerParty(B.CalcStat(B.partyReader()));
		List<Pokemon> PlayerTeam = B.getPlayerParty();
		B.setOpponentParty(B.CalcStat(B.opponentReader()));
		List<Pokemon> OpponentTeam = B.getOpponentParty();
		/*for (Pokemon p : PlayerTeam) {
			System.out.println(p);
		}*/
		System.out.println(PlayerTeam.get(0));
		System.out.println(OpponentTeam.get(0));
		
		Player Player1 = new Player (PlayerTeam);
		if (x == 1) {
			EasyAI Player2 = new EasyAI(OpponentTeam, Player1);
			Battle newBattle = new Battle (Player1, Player2);
			newBattle.battlePhase();
		} else if (x == 2) {
			NormalAI Player2 = new NormalAI(OpponentTeam, Player1);
			Battle newBattle = new Battle (Player1, Player2);
			newBattle.battlePhase();
		}
	}

}
