package com.Data;

import java.util.*;
import java.io.*;
	
/**************************************************************
 * This Class use to handle Data of Pokemon (kind of PokeDex) *
 * Also modifier for Party Pokemon							  *
 **************************************************************/

public class Pokemon {
	private String vDexName, vPartyName;
	private List<String> vLearnSet;
	private List<Moves> vPartyMoves;
	private List<Type> vPartyType;
	private int vBHP,vBATK,vBDEF,vBSPA,vBSPD,vBSPE;
	private int vIVHP,vIVATK,vIVDEF,vIVSPA,vIVSPD,vIVSPE;
	private int vEVHP,vEVATK,vEVDEF,vEVSPA,vEVSPD,vEVSPE;
	private int vTHP,vTATK,vTDEF,vTSPA,vTSPD,vTSPE;
	private int vMHP;
	private double vRHP;
	private int vLvl;
	
	public Pokemon(String pDexName) {
		this.vDexName = pDexName;
	}
	public Pokemon(String pPartyName, int pLvl) {
		this.vPartyName = pPartyName;
		this.vLvl = pLvl;
	}
	
	public void setBHP(int pBHP) {
		this.vBHP = pBHP;
	}
	public void setBATK(int pBATK) {
		this.vBATK = pBATK;
	}
	public void setBDEF(int pBDEF) {
		this.vBDEF = pBDEF;
	}
	public void setBSPA(int pBSPA) {
		this.vBSPA = pBSPA;
	}
	public void setBSPD(int pBSPD) {
		this.vBSPD = pBSPD;
	}
	public void setBSPE(int pBSPE) {
		this.vBSPE = pBSPE;
	}
	public void setIVHP(int pIVHP) {
		this.vIVHP = pIVHP;
	}
	public void setIVATK(int pIVATK) {
		this.vIVATK = pIVATK;
	}
	public void setIVDEF(int pIVDEF) {
		this.vIVDEF = pIVDEF;
	}
	public void setIVSPA(int pIVSPA) {
		this.vIVSPA = pIVSPA;
	}
	public void setIVSPD(int pIVSPD) {
		this.vIVSPD = pIVSPD;
	}
	public void setIVSPE(int pIVSPE) {
		this.vIVSPE = pIVSPE;
	}
	public void setEVHP(int pEVHP) {
		this.vEVHP = pEVHP;
	}
	public void setEVATK(int pEVATK) {
		this.vEVATK = pEVATK;
	}
	public void setEVDEF(int pEVDEF) {
		this.vEVDEF = pEVDEF;
	}
	public void setEVSPA(int pEVSPA) {
		this.vEVSPA = pEVSPA;
	}
	public void setEVSPD(int pEVSPD) {
		this.vEVSPD = pEVSPD;
	}
	public void setEVSPE(int pEVSPE) {
		this.vEVSPE = pEVSPE;
	}
	public void setTHP(int pTHP) {
		this.vTHP = pTHP;
	}
	public void setTATK(int pTATK) {
		this.vTATK = pTATK;
	}
	public void setTDEF(int pTDEF) {
		this.vTDEF = pTDEF;
	}
	public void setTSPA(int pTSPA) {
		this.vTSPA = pTSPA;
	}
	public void setTSPD(int pTSPD) {
		this.vTSPD = pTSPD;
	}
	public void setTSPE(int pTSPE) {
		this.vTSPE = pTSPE;
	}
	public void setMHP(int pMHP) {
		this.vMHP = pMHP;
	}
	public void setRHP(double pRHP) {
		this.vRHP = pRHP;
	}
	public void setPartyType(List<Type> pType) {
		this.vPartyType = pType;
	}
	public void setLearnSet(List<String> pLearnSet) {
		this.vLearnSet = pLearnSet;
	}
	public void setPartyMoves(List<Moves> pMoves) {
		this.vPartyMoves = pMoves;
	}
	
	public int getBHP() {
		return vBHP;
	}
	public int getBATK() {
		return vBATK;
	}
	public int getBDEF() {
		return vBDEF;
	}
	public int getBSPA() {
		return vBSPA;
	}
	public int getBSPD() {
		return vBSPD;
	}
	public int getBSPE() {
		return vBSPE;
	}
	public int getIVHP() {
		return vIVHP;
	}
	public int getIVATK() {
		return vIVATK;
	}
	public int getIVDEF() {
		return vIVDEF;
	}
	public int getIVSPA() {
		return vIVSPA;
	}
	public int getIVSPD() {
		return vIVSPD;
	}
	public int getIVSPE() {
		return vIVSPE;
	}
	public int getEVHP() {
		return vEVHP;
	}
	public int getEVATK() {
		return vEVATK;
	}
	public int getEVDEF() {
		return vEVDEF;
	}
	public int getEVSPA() {
		return vEVSPA;
	}
	public int getEVSPD() {
		return vEVSPD;
	}
	public int getEVSPE() {
		return vEVSPE;
	}
	public int getTHP() {
		return vTHP;
	}
	public int getTATK() {
		return vTATK;
	}
	public int getTDEF() {
		return vTDEF;
	}
	public int getTSPA() {
		return vTSPA;
	}
	public int getTSPD() {
		return vTSPD;
	}
	public int getTSPE() {
		return vTSPE;
	}
	public int getMHP() {
		return vMHP;
	}
	public double getRHP() {
		return vRHP;
	}
	public int getLevel() {
		return vLvl;
	}
	public String getDexName() {
		return vDexName;
	}
	public String getPartyName() {
		return vPartyName;
	}
	public List<String> getLaernSet(){
		return vLearnSet;
	}
	public List<Type> getPartyType(){
		return vPartyType;
	}
	public List<Moves> getPartyMoves(){
		return vPartyMoves;
	}
	
	@Override
	public String toString() {
		List<String> t = new ArrayList<>();
		List<String> m = new ArrayList<>();
		for (int i = 0; i < vPartyType.size(); i++) {
			t.add(vPartyType.get(i).getType());
		}
		for (int i = 0; i < vPartyMoves.size(); i++) {
			m.add(vPartyMoves.get(i).getMoves());
		}
		return "Name: " + vPartyName +
				"\nLevel: " + vLvl +
				"\nType: " + t +
				"\nStats: " + vTHP + " " + vTATK + " " + vTDEF + " " + vTSPA + " " + vTSPD + " " + vTSPE + 
				"\nMoves: " + m;
				
	}
	
	public static List<Pokemon> readPokemon(String filename) throws IOException{
		List<Pokemon> tDexList = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(filename))){
			String line;
			Pokemon pokemon = null;
			while((line = br.readLine()) != null) {
				String[] parts = line.split(":");
				switch(parts[0]) {
				case "Name":
					if (pokemon != null) {
						tDexList.add(pokemon);
					}
					pokemon = new Pokemon(parts[1]);
					break;
				case "Stat":
					int[] stats = Arrays.stream(parts[1].split(",")).mapToInt(Integer::parseInt).toArray();
					pokemon.setBHP(stats[0]);
					pokemon.setBATK(stats[1]);
					pokemon.setBDEF(stats[2]);
					pokemon.setBSPA(stats[3]);
					pokemon.setBSPD(stats[4]);
					pokemon.setBSPE(stats[5]);
					break;
				case "Moves":
					String[] moves = parts[1].split(",");
					pokemon.setLearnSet(Arrays.asList(moves));
					break;
					default:
						throw new IllegalArgumentException("Invalid input: " + parts[0]);
				}
			}
			if (pokemon != null) {
				tDexList.add(pokemon);
			}
		}
		
		return tDexList;
	}
}
