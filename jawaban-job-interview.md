Telah dipindahkan ke : [repository lain](https://gitlab.com/arashikoyuki8/pbo/-/blob/main/jawaban-job-interview.md)

# 1

# 2
untuk menyelesaikan solusi pada pertanyaan pertama, disini kita akan membuat beberapa class.
- pertama, class Pokemon untuk me-read Pokemon (bekerja seperti Pokedex)
- kedua, class untuk mendefinisikan move dan type.
- ketiga, class untuk party
- keempat, class untuk AI dan Player
- kelima, class Battle

[Link (all source code)](https://github.com/DanKoyuki/PBO-Tugas/tree/main/pokemon_Battle_Simulator/src/com)
# 3
OOP atau object oriented programming merupakan suatu cara menulis kode dimana setiap kode ditulis dengan tujuan untuk menciptakan suatu objek. terdapat beberapa pilar pada oop:
- Abstraction (menyembunyikan detail), suatu class yang biasanya hanya berisi declarasi method.. abstract class tidak dapat dibuat menjadi objek tetapi dapat di inherite pada kelas lain untuk menentukan behaviour dari kelas tersebut. sehingga method yang ada pada abstract class akan mendapatkan implementasi pada subclassnya. Abstraction pada bahasa java biasanya ditandai dengan keyword 'abstract'
- Inheritance (mewariskan), menambahkan fungsi dari suatu class pada class lain. more like if a parent have something, that thing will also belong to his son. Inheritance ditandai dengan keyword 'extends', dapat juga menggunakan 'implements' jika yang akan di-include adalah class interface (jenis class abstract tetapi hanya boleh ada unimplemented method dan bersifat final)
- Encapsulation (mengurung), membuat suatu class, fungsi, atau attribute menjadi lebih protective(?) suatu class yang menerapkan encapsulation memiliki ciri dimana attribute atau methodnya memiliki modifier Public, Private atau protected.
- Polymorph (BERUBAH), same but different... terdapat dua keyword untuk Polymorph, yaitu Overload dan Override (biasanya menjadi annotation). Overload adalah 2 atau lebih method dengan nama yang sama tetapi memiliki argument (parameter) yang berbeda (terutama jumlahnya). Override adalah 2 atau lebih method dengan nama yang sama tetapi memiliki pengimplementasian yang berbeda.
# 4 Encapsulation
Encapsulation commonly used to attribute (variable), as attribute is the weakest part of code, it can be a loophole, miused and such... its can be edited easily so we need to protect it..
Hierarchy:
Private fields are example of Strong Encapsulation.
Public fields are example of Weak Encapsulation. In this case fields are still encapsulated in the class, but visible to outside world.
Protected fields show moderate Encapsulation.

[Link](https://github.com/DanKoyuki/PBO-Tugas/blob/main/pokemon_Battle_Simulator/src/com/Data/Pokemon.java)
# 5 Abstraction
as the program was Pokemon Battle Simulator, this a bit hard to search for something similar in code... as the result the abstraction on this program was the Difficulty of the AI, which im devide them to different class to make switching mode easier. Even with that, its still hard to find abstraction as the AI didnt use much method... (its was told to be easier method for beginner)
[Link](https://github.com/DanKoyuki/PBO-Tugas/blob/main/pokemon_Battle_Simulator/src/com/User/DifficultyAI.java)
# 6  Inheritance dan Polymorph
same as abstraction, there almost nothing to inherit except the abstraction class.. each move and type are made to object so other class just need to create the object to use it.
for polymorph itself, its differ the behaviour of AI through Overriding.
[Link AI1](https://github.com/DanKoyuki/PBO-Tugas/blob/main/pokemon_Battle_Simulator/src/com/User/EasyAI.java)
[Link AI2](coming soon)
# 7 Use Case
| Function | Priority | Class |
|----------|----------|-------|
| Pokemon can Battle | 10 | [Battle](https://github.com/DanKoyuki/PBO-Tugas/blob/main/pokemon_Battle_Simulator/src/com/Main/Battle.java) |
| Read Pokemon (as Pokedex, its not show though) | 9 | [Pokemon](https://github.com/DanKoyuki/PBO-Tugas/blob/main/pokemon_Battle_Simulator/src/com/Data/Pokemon.java) |
| Read Move & Type | 9 | [Moves](https://github.com/DanKoyuki/PBO-Tugas/blob/main/pokemon_Battle_Simulator/src/com/Data/Moves.java) and [Type](https://github.com/DanKoyuki/PBO-Tugas/blob/main/pokemon_Battle_Simulator/src/com/Data/Type.java)
| Read Party (now its in form of .txt file) | 9 | [Party](https://github.com/DanKoyuki/PBO-Tugas/blob/main/pokemon_Battle_Simulator/src/com/Data/Party.java)
| apply battle field effect (statuseffect,secondeffect from moves, field terrain and weather, and other | 7 |
| apply abilities | 7 |
| applies Item | 8 |
# 8 Class Diagram

# 9 YT Video

# 10 UX/UI
(not Done & CLI move are Demo)

![CLI](https://gitlab.com/arashikoyuki8/PBO-Tugas/-/blob/main/GIF/Demo.gif)
![UI](https://gitlab.com/arashikoyuki8/PBO-Tugas/-/blob/main/GIF/DemoAPP.gif)
