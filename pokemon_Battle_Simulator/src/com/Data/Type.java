package com.Data;

import java.io.*;
import java.util.*;

public class Type {
	private String vTypeName;
	private List<String> vSuperEffect, vLessEffect, vNoEffect, vNeutral;
	
	public Type(String pTypeName) {
		this.vTypeName = pTypeName;
	}
	public void setSuperEffect(List<String> pSuperEffect) {
		this.vSuperEffect = pSuperEffect;
	}
	public void setLessEffect(List<String> pLessEffect) {
		this.vLessEffect = pLessEffect;
	}
	public void setNoEffect(List<String> pNoEffect) {
		this.vNoEffect = pNoEffect;
	}
	public void setNeutral(List<String> pNeutral) {
		this.vNeutral = pNeutral;
	}
	
	public String getType() {
		return vTypeName;
	}
	public List<String> getSuperEffect() {
		return vSuperEffect;
	}
	public List<String> getLessEffect() {
		return vLessEffect;
	}
	public List<String> getNoEffect() {
		return vNoEffect;
	}
	public List<String> getNeutral(){
		return vNeutral;
	}
	
	public static List<Type> readType(String filename) throws IOException{
		List<Type> tTypeAdvantage = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(filename))){
			String line;
			Type type = null;
			while((line = br.readLine()) != null) {
				String[] parts = line.split(":");
				switch(parts[0]) {
				case "Name":
					if (type != null) {
						tTypeAdvantage.add(type);
					}
					type = new Type(parts[1]);
					break;
				case "Effective":
					String[] Effective = parts[1].split(",");
					type.setSuperEffect(Arrays.asList(Effective));
					break;
				case "Resist":
					String[] Resist = parts[1].split(",");
					type.setLessEffect(Arrays.asList(Resist));
					break;
				case "noEffect":
					String[] noEffect = parts[1].split(",");
					type.setNoEffect(Arrays.asList(noEffect));
					break;
				case "Neutral":
					String[] Neutral = parts[1].split(",");
					type.setNeutral(Arrays.asList(Neutral));
					break;
					default:
						throw new IllegalArgumentException("Invalid input: "+parts[0]);
				}
			}
			if (type != null) {
				tTypeAdvantage.add(type);
			}
		}
		
		return tTypeAdvantage;
	}
}
