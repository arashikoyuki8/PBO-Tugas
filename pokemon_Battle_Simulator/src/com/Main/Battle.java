package com.Main;

import com.User.*;

public class Battle {
	private int vTurnCount;
	private boolean vEndGame;
	private Player vP1;
	private DifficultyAI vP2;
	
	public Battle (Player pTeam1, DifficultyAI pTeam2) {
		this.vTurnCount = 1;
		this.vP1 = pTeam1;
		this.vP2 = pTeam2;
	}
	
	public void battlePhase() {
		if (vTurnCount == 1) {
			vP1.mPlayerCPokemon();
			vP2.opponentCPokemon();
		}
		if (vP1.getPokemon().getRHP() == 0) {
			vP1.mPlayerCPokemon();
		} else if (vP2.getPokemon().getRHP() == 0) {
			vP2.opponentCPokemon();
		}
		System.out.println("=======================================================================================");
		System.out.println(vP1.getPokemon().getPartyName() + "								" + vP2.getPokemon().getPartyName());
		System.out.println("HP: " + vP1.getPokemon().getRHP() +" / "+ vP1.getPokemon().getMHP() + "								" + "HP: " + vP2.getPokemon().getRHP() +" / "+ vP2.getPokemon().getMHP());
		System.out.println("=======================================================================================");
		vP1.mPlayerDecision(vP2.getPokemon());
		vP2.opponentDecision(vP1.getPokemon());
		if (vP1.getPokemon().getTSPE() >= vP2.getPokemon().getTSPE()) {
			vP1.mPlayerAction();
			vP2.mOpponentAction();
		} else {
			vP2.mOpponentAction();
			vP1.mPlayerAction();
		}
		this.endGame();
	}
	public void battleOutcome() {
		if (vP1.mIsWin() == false){
			System.out.println("AI win the Battle");
			this.vEndGame = true;
		} else if (vP2.mIsWin() == false) {
			System.out.println("Player win the Battle");
			this.vEndGame = true;
		}
	}
	public void endGame() {
		this.battleOutcome();
		if (vEndGame) {
			System.out.println("Game has end!!");
		} else {
			this.vTurnCount++;
			this.battlePhase();
		}
	}
}
