package com.User;

import com.Data.*;

public abstract class DifficultyAI {
	public abstract Pokemon opponentCPokemon();
    public abstract Moves opponentCMove(Pokemon p1);
    public abstract Pokemon opponentSwitch(Pokemon p1);
    public abstract void opponentDecision(Pokemon p1);
	public abstract Pokemon getPokemon();
	public abstract void mApplyDamage(Moves move);
	public abstract void mOpponentAction();
	public abstract void mSwitchPokemon(Pokemon pokemon);
	public abstract boolean mIsWin();
}
