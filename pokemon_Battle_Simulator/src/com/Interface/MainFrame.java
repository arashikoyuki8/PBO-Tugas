package com.Interface;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import com.Data.DataReader;
import com.Data.Pokemon;
import com.User.*;
import java.awt.Font;
import java.awt.Image;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.net.URISyntaxException;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Insets;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;

public class MainFrame {

	private JFrame frmIrwantoProject;
	private Player vPlayer;
	private DifficultyAI vAI;
	private ChooseFrame vCF;
	private JButton bX, bInfo;
	private JTextPane Info;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame window = new MainFrame();
					window.frmIrwantoProject.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws URISyntaxException 
	 */
	public MainFrame() throws URISyntaxException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws URISyntaxException 
	 */
	private void initialize() throws URISyntaxException {
		
		DataReader B = new DataReader();
		B.setPlayerParty(B.CalcStat(B.partyReader()));
		List<Pokemon> PlayerTeam = B.getPlayerParty();
		B.setOpponentParty(B.CalcStat(B.opponentReader()));
		List<Pokemon> OpponentTeam = B.getOpponentParty();
		this.vPlayer = new Player(PlayerTeam);
		this.vAI = new EasyAI(OpponentTeam, vPlayer);
		vCF = new ChooseFrame(vPlayer, vAI);
		
		frmIrwantoProject = new JFrame();
		ImageIcon icons = new ImageIcon(this.getClass().getResource("/Icon2.png"));
		frmIrwantoProject.setIconImage(icons.getImage());
		frmIrwantoProject.setTitle("Irwanto Danang Bahtiar");
		frmIrwantoProject.setResizable(false);
		frmIrwantoProject.setBounds(100, 100, 659, 398);
		frmIrwantoProject.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmIrwantoProject.getContentPane().setLayout(null);
		
		
		bInfo = new JButton("INFO");
		bInfo.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		bInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Info.setVisible(true);
				bX.setVisible(true);
				bInfo.setVisible(false);
			}
		});
		bInfo.setBounds(544, 325, 89, 23);
		frmIrwantoProject.getContentPane().add(bInfo);
		
		bX = new JButton("X");
		bX.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		bX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bInfo.setVisible(true);
				Info.setVisible(false);
				bX.setVisible(false);
			}
		});
		bX.setMargin(new Insets(0, 0, 0, 0));
		bX.setBounds(600, 233, 33, 23);
		frmIrwantoProject.getContentPane().add(bX);
		bX.setVisible(false);
		
		Info = new JTextPane();
		Info.setBorder(new LineBorder(new Color(0, 105, 210), 3));
		Info.setFont(new Font("Nirmala UI Semilight", Font.BOLD, 15));
		Info.setContentType("");
		Info.setText("Name   : Irwanto Danang Bahtiar\nNIM      : 1217050070\nSubject: Object Oriented Programming");
		Info.setBounds(348, 240, 285, 69);
		frmIrwantoProject.getContentPane().add(Info);
		Info.setVisible(false);
		
		
		JLabel GameTitle = new JLabel("");
		GameTitle.setHorizontalAlignment(SwingConstants.CENTER);
		Image Logo = new ImageIcon(this.getClass().getResource("/Logo.png")).getImage();
		GameTitle.setIcon(new ImageIcon(Logo));
		GameTitle.setForeground(new Color(193, 194, 180));
		GameTitle.setFont(new Font("French Script MT", Font.BOLD | Font.ITALIC, 45));
		GameTitle.setBounds(170, 11, 325, 112);
		frmIrwantoProject.getContentPane().add(GameTitle);
		
		JButton bPlay = new JButton("");
		bPlay.setMargin(new Insets(0, 0, 0, 0));
		bPlay.setIconTextGap(0);
		bPlay.setToolTipText("Battle");
		Image bBattle = new ImageIcon(this.getClass().getResource("/Battle.png")).getImage();
		bPlay.setIcon(new ImageIcon(bBattle));
		bPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmIrwantoProject.dispose();
				vCF.setVisible(true);
			}
		});
		bPlay.setFont(new Font("Gill Sans MT", Font.BOLD, 18));
		bPlay.setBounds(73, 181, 125, 40);
		frmIrwantoProject.getContentPane().add(bPlay);
		
		JButton bExit = new JButton("");
		bExit.setMargin(new Insets(0, 0, 0, 0));
		bExit.setIconTextGap(0);
		bExit.setToolTipText("Exit Windows");
		Image bexit = new ImageIcon(this.getClass().getResource("/EXIT.png")).getImage();
		bExit.setIcon(new ImageIcon(bexit));
		bExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		bExit.setFont(new Font("Gill Sans MT", Font.BOLD, 18));
		bExit.setBounds(73, 269, 125, 40);
		frmIrwantoProject.getContentPane().add(bExit);
		
		JLabel Background = new JLabel("");
		Image bg = new ImageIcon(this.getClass().getResource("/BG.png")).getImage();
		Background.setIcon(new ImageIcon(bg));
		Background.setBounds(0, 0, 650, 366);
		frmIrwantoProject.getContentPane().add(Background);
	}
}
