package com.Interface;

import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.User.DifficultyAI;
import com.User.Player;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

@SuppressWarnings("serial")
public class ChooseFrame extends JFrame {

	private JPanel contentPane;
	private Player vPlayer;
	private DifficultyAI vAI;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChooseFrame frame = new ChooseFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public ChooseFrame(Player p1, DifficultyAI p2) {
		setTitle("Irwanto Danang Bahtiar");
		this.vPlayer = p1;
		this.vAI = p2;
		ImageIcon icons = new ImageIcon(this.getClass().getResource("/Icon2.png"));
		setIconImage(icons.getImage());
		Initialize();
	}

	public void Initialize() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 659, 398);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		int index = vPlayer.mGetTeam().length;
		
		if (index != 0 && index >= 1) {
			JButton bPoke1 = new JButton(vPlayer.mGetTeam()[0].getPartyName());
			bPoke1.setBounds(70, 80, 159, 55);
			bPoke1.setFont(new Font("Franklin Gothic Demi Cond", Font.PLAIN, 20));
			bPoke1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					vPlayer.guiChoosePokemon(0);
					vAI.opponentCPokemon();
					setVisible(false);
					BattleFrame battle = new BattleFrame(vPlayer, vAI);
					battle.setVisible(true);
				}
			});
			bPoke1.setForeground(Color.BLACK);
			getContentPane().add(bPoke1);
		}
		if (index != 1 && index >= 2) {
			JButton bPoke2 = new JButton(vPlayer.mGetTeam()[1].getPartyName());
			bPoke2.setBounds(416, 80, 159, 55);
			bPoke2.setFont(new Font("Franklin Gothic Demi Cond", Font.PLAIN, 20));
			Image icons = new ImageIcon(this.getClass().getResource("/Icon-"+vPlayer.mGetTeam()[1].getPartyName()+".png")).getImage();
			bPoke2.setIcon(new ImageIcon(icons));
			bPoke2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					vPlayer.guiChoosePokemon(1);
					vAI.opponentCPokemon();
					setVisible(false);
					BattleFrame battle = new BattleFrame(vPlayer, vAI);
					battle.setVisible(true);
				}
			});
			getContentPane().add(bPoke2);
		}
		if (index != 2 && index >= 3) {
			JButton bPoke3 = new JButton(vPlayer.mGetTeam()[2].getPartyName());
			bPoke3.setBounds(70, 175, 159, 55);
			bPoke3.setFont(new Font("Franklin Gothic Demi Cond", Font.PLAIN, 20));
			bPoke3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					vPlayer.guiChoosePokemon(2);
					vAI.opponentCPokemon();
					setVisible(false);
					BattleFrame battle = new BattleFrame(vPlayer, vAI);
					battle.setVisible(true);
				}
			});
			getContentPane().add(bPoke3);
		}
		if (index != 3 && index >= 4) {
			JButton bPoke4 = new JButton(vPlayer.mGetTeam()[3].getPartyName());
			bPoke4.setBounds(416, 175, 159, 55);
			bPoke4.setFont(new Font("Franklin Gothic Demi Cond", Font.PLAIN, 20));
			getContentPane().add(bPoke4);
		}
		if (index != 4 && index >= 5) {
			JButton bPoke5 = new JButton(vPlayer.mGetTeam()[4].getPartyName());
			bPoke5.setBounds(70, 268, 159, 55);
			bPoke5.setFont(new Font("Franklin Gothic Demi Cond", Font.PLAIN, 20));
			getContentPane().add(bPoke5);
		}
		if (index != 5 && index >= 6) {
			JButton bPoke6 = new JButton(vPlayer.mGetTeam()[5].getPartyName());
			bPoke6.setBounds(416, 268, 159, 55);
			bPoke6.setFont(new Font("Franklin Gothic Demi Cond", Font.PLAIN, 20));
			getContentPane().add(bPoke6);
		}
		JLabel Title = new JLabel("Choose Your Pokemon!!");
		Title.setForeground(new Color(0, 255, 0));
		Title.setBounds(156, 11, 328, 48);
		Title.setFont(new Font("French Script MT", Font.BOLD | Font.ITALIC, 40));
		getContentPane().add(Title);

		JButton bBack = new JButton("");
		bBack.setMargin(new Insets(0, 0, 0, 0));
		bBack.setIconTextGap(0);
		bBack.setToolTipText("Back to Main Menu");
		Image iBack = new ImageIcon(this.getClass().getResource("/bBack2.png")).getImage();
		bBack.setIcon(new ImageIcon(iBack));
		bBack.setBounds(280, 325, 89, 23);
		bBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				MainFrame.main(null);
			}
		});
		contentPane.add(bBack);
		
		Image cBg = new ImageIcon(this.getClass().getResource("/chooseBg3.png")).getImage();
		JLabel BackgroundChoose = new JLabel("");
		BackgroundChoose.setBounds(0, 0, 643, 359);
		BackgroundChoose.setIcon(new ImageIcon(cBg));
		contentPane.add(BackgroundChoose);
	}
}
