Name:Charizard,75
Stats:31,31,31,31,31,31,4,0,0,252,0,252
Moves:Flamethrower,Solar Beam,Air Slash,Fire Blast
Type:Fire,Flying
Name:Ivysaur,75
Stats:31,31,31,31,31,31,4,0,0,252,0,252
Moves:Solar Beam,Sludge Bomb,Giga Drain,Mega Drain
Type:Grass,Poison