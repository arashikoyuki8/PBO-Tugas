package com.User;

import com.Data.Moves;
import com.Data.Pokemon;
import com.Data.Type;

import java.util.*;

public class Player {
	private Pokemon[] vPlayerTeam;
	private Pokemon vCurrentPokemon;
	private Moves[] vPlayerMoves;
	private Scanner sc;
	private Moves choosenMove;
	private Pokemon vOpponentPokemon;
	private Pokemon choosenPokemon;
	private int actionToken;
	private double vDamageCount;
	private boolean isWin;
	private int isFainted;
	
	public Player(List<Pokemon> pTeam) {
		this.vPlayerTeam = new Pokemon[pTeam.size()];
		for (int i = 0; i < pTeam.size(); i++) {
			this.vPlayerTeam[i] = pTeam.get(i);
		}
		for (int i = 0; i < vPlayerTeam.length; i++) {
			vPlayerTeam[i].setMHP(vPlayerTeam[i].getTHP());
			vPlayerTeam[i].setRHP(vPlayerTeam[i].getTHP());
		}
		sc = new Scanner(System.in);
		this.isWin = true;
	}
	
	//CLI Method (some are used on interface implementation
	public Pokemon getPokemon() {
		return vCurrentPokemon;
	}
	public void mPlayerDecision(Pokemon p2) {
		this.vOpponentPokemon = p2;
		System.out.println("			  What will "+vCurrentPokemon.getPartyName()+" do? (1 or 2)");
		System.out.println("   			  1. Fight              2. Switch        ");
		System.out.println("=======================================================================================");
		int c = sc.nextInt();
		if (c == 1) {
			this.mPlayerCMoves();
		} else if (c == 2) {
			this.mPlayerSwitch();
		} else {
			System.out.println("Invalid Choice!!");
			this.mPlayerDecision(p2);
		}
	}
	public void mPlayerAction() {
		if (this.vCurrentPokemon.getRHP() != 0) {
			if (this.actionToken == 1) {
				this.mApplyDamage(choosenMove, vOpponentPokemon);
			} else {
				this.mSwitchPokemon(choosenPokemon);
			}
		} else {
			for (Pokemon p : vPlayerTeam) {
				if (p.getRHP() == 0) {
					isFainted++;
				}
			}
			if (isFainted != vPlayerTeam.length) {
				this.isFainted = 0;
				this.mPlayerCPokemon();
			} else if (isFainted == vPlayerTeam.length){
				this.isWin = false;
			}
		}
	}
	public Pokemon mPlayerCPokemon () {
		System.out.println("Choose your Pokemon!!");
		for (int i = 0; i < vPlayerTeam.length; i++) {
			System.out.println(i+1 + ". "+ vPlayerTeam[i].getPartyName());
		}
		System.out.print("Inpur Number: ");
		int c = sc.nextInt();
		if (vPlayerTeam[c-1].getRHP() == 0) {
			System.out.println("Pokemon has Fainted!!");
			this.mPlayerCPokemon();
		} else {
			this.vCurrentPokemon = vPlayerTeam[c-1];
		}
		return vCurrentPokemon;
	}
	
	public Moves mPlayerCMoves() {
		this.vPlayerMoves = new Moves[vCurrentPokemon.getPartyMoves().size()];
		List<Moves> move = vCurrentPokemon.getPartyMoves();
		for (int i = 0; i < vPlayerMoves.length; i++) {
			vPlayerMoves[i] = move.get(i);
		}
		System.out.println("Choose Moves (1-4, 5 to cancel):");
		for (int i = 0; i < vPlayerMoves.length; i++) {
			System.out.println(i+1+". "+vPlayerMoves[i].getMoves());
		}
		System.out.println("=======================================================================================");
		int c = sc.nextInt();
		switch (c) {
		case 1:
		case 2:
		case 3:
		case 4:
			this.choosenMove = vPlayerMoves[c-1];
			this.actionToken = 1;
			break;
		case 5:
			this.mPlayerDecision(vOpponentPokemon);
			break;
			default:
				System.out.println("Invalid Moves!!");
				this.mPlayerCMoves();
		}
		return choosenMove;
	}
	
	public void mApplyDamage(Moves move, Pokemon p2) {
	    double tDamageCount;
	    if (move.IsSpecial()) {
	        tDamageCount = (((2 * vCurrentPokemon.getLevel() / 5) + 2) * move.getMovePower() * vCurrentPokemon.getTSPA() / p2.getTSPD()) / 50 + 2;
	    } else {
	        tDamageCount = (((2 * vCurrentPokemon.getLevel() / 5) + 2) * move.getMovePower() * vCurrentPokemon.getTATK() / p2.getTDEF()) / 50 + 2;
	    }
	    this.vDamageCount = tDamageCount;
	    if (vCurrentPokemon.getPartyType().contains(move.getMoveType())) {
	        this.vDamageCount *= 1.5;
	    }

	    for (Type t : p2.getPartyType()) {
	        if (t.getSuperEffect().contains(move.getMoveType().getType())) {
	            this.vDamageCount *= 2;
	        } else if (t.getLessEffect().contains(move.getMoveType().getType())) {
	            this.vDamageCount *= 0.5;
	        } else if (t.getNoEffect().contains(move.getMoveType().getType())) {
	            this.vDamageCount = 0;
	        }
	    }

	    System.out.println(vCurrentPokemon.getPartyName() + " Use " + move.getMoves());
	    System.out.println(p2.getPartyName() + " Takes " + vDamageCount + " Damage!");

	    if (p2.getRHP() <= vDamageCount) {
	        p2.setRHP(0);
	    } else {
	        p2.setRHP(p2.getRHP() - vDamageCount);
	    }
	}
	
	public void mPlayerSwitch() {
		System.out.println("Choose Pokemon to Switch!!");
		for (int i = 0; i < vPlayerTeam.length; i++) {
			System.out.println(i+1 + ". "+ vPlayerTeam[i].getPartyName());
		}
		System.out.print("Inpur Number (1-6, 7 to cancel): ");
		int c = sc.nextInt();
		switch(c) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			if (vPlayerTeam[c-1].getRHP() == 0 || vPlayerTeam[c-1].getPartyName().equals(vCurrentPokemon.getPartyName())) {
				System.out.println("Cant use this Pokemon!!");
				this.mPlayerSwitch();
			} else {
				this.choosenPokemon = vPlayerTeam[c-1];
				this.actionToken = 0;
				this.mPlayerAction();
			}
			break;
		case 7:
			this.choosenPokemon = vCurrentPokemon;
			this.mPlayerDecision(vOpponentPokemon);
			break;
			default:
				System.out.println("Invalid input!!");
				this.mPlayerSwitch();
		}
	}
	
	public void mSwitchPokemon(Pokemon pokemon) {
		this.vCurrentPokemon = pokemon;
	}	
	public boolean mIsWin() {
		return isWin;
	}
	
	// UX/UI Method
	public Pokemon[] mGetTeam() {
		return vPlayerTeam;
	}
	public Moves guiGetMove() {
		return choosenMove;
	}
	
	public void guiChoosePokemon(int y) {
		this.vCurrentPokemon = vPlayerTeam[y];
	}
	
	public Moves guiChooseMove(int y) {
		this.vPlayerMoves = new Moves[vCurrentPokemon.getPartyMoves().size()];
		List<Moves> move = vCurrentPokemon.getPartyMoves();
		for (int i = 0; i < vPlayerMoves.length; i++) {
			vPlayerMoves[i] = move.get(i);
		}
		this.choosenMove = vPlayerMoves[y];
		return choosenMove;
	}
	
	public void guiApplyDamage(Moves move, Pokemon p2) {
	    double tDamageCount;
	    if (move.IsSpecial()) {
	        tDamageCount = (((2 * vCurrentPokemon.getLevel() / 5) + 2) * move.getMovePower() * vCurrentPokemon.getTSPA() / p2.getTSPD()) / 50 + 2;
	    } else {
	        tDamageCount = (((2 * vCurrentPokemon.getLevel() / 5) + 2) * move.getMovePower() * vCurrentPokemon.getTATK() / p2.getTDEF()) / 50 + 2;
	    }
	    this.vDamageCount = tDamageCount;
	    if (vCurrentPokemon.getPartyType().contains(move.getMoveType())) {
	        this.vDamageCount *= 1.5;
	    }

	    for (Type t : p2.getPartyType()) {
	        if (t.getSuperEffect().contains(move.getMoveType().getType())) {
	            this.vDamageCount *= 2;
	        } else if (t.getLessEffect().contains(move.getMoveType().getType())) {
	            this.vDamageCount *= 0.5;
	        } else if (t.getNoEffect().contains(move.getMoveType().getType())) {
	            this.vDamageCount = 0;
	        }
	    }
	    if (p2.getRHP() <= vDamageCount) {
	        p2.setRHP(0);
	    } else {
	        p2.setRHP(p2.getRHP() - vDamageCount);
	    }
	}
	public void guiPlayerAction(Pokemon p2) {
		vOpponentPokemon = p2;
		if (this.vCurrentPokemon.getRHP() != 0) {
			this.guiApplyDamage(choosenMove, vOpponentPokemon);
		} else {
			for (Pokemon p : vPlayerTeam) {
				if (p.getRHP() == 0) {
					isFainted++;
				}
			}
			if (isFainted != vPlayerTeam.length) {
				this.isFainted = 0;
				this.mPlayerCPokemon();
			} else if (isFainted == vPlayerTeam.length){
				this.isWin = false;
			}
		}
	}
}
