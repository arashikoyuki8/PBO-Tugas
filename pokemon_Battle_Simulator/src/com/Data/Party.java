package com.Data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Party {
	public static List<Pokemon> readParty(String filename) throws IOException, URISyntaxException{
		List<Pokemon> Team = new ArrayList<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(filename))){
			String line;
			Pokemon teamMember = null;
			DataReader dr = new DataReader();
			List<Pokemon> dexList = dr.pokeReader();
			List<Moves> moveList = dr.moveReader();
			List<Type> typeList = dr.typeReader();
			while ((line = br.readLine()) != null) {
				String[] parts = line.split(":");
				switch (parts[0]) {
				case "Name":
					String[] base = parts[1].split(",");
					String newPokemon = base[0];
					int level = Integer.parseInt(base[1]);
					for (Pokemon p : dexList) {
						if (p.getDexName().equals(newPokemon)) {
							if (teamMember != null) {
								Team.add(teamMember);
							}
							teamMember = new Pokemon(newPokemon, level);
							break;
						}
					}
					break;
				case "Stats":
					int[] stats = Arrays.stream(parts[1].split(",")).mapToInt(Integer::parseInt).toArray();
					teamMember.setIVHP(stats[0]);
					teamMember.setIVATK(stats[1]);
					teamMember.setIVDEF(stats[2]);
					teamMember.setIVSPA(stats[3]);
					teamMember.setIVSPD(stats[4]);
					teamMember.setIVSPE(stats[5]);
					teamMember.setEVHP(stats[6]);
					teamMember.setEVATK(stats[7]);
					teamMember.setEVDEF(stats[8]);
					teamMember.setEVSPA(stats[9]);
					teamMember.setEVSPD(stats[10]);
					teamMember.setEVSPE(stats[11]);
					break;
				case "Moves":
					String[] moves = parts[1].split(",");
					List<Moves> move = new ArrayList<>();
					for (int i = 0; i < moves.length; i++) {
						for (Moves m : moveList) {
							if (m.getMoves().equals(moves[i])) {
								move.add(m);
								break;
							}
						}
						teamMember.setPartyMoves(move);
					}
					break;
				 case "Type":
					   List<Type> Type = new ArrayList<>();
					   String[] type = parts[1].split(",");
					   for (int i = 0; i < type.length; i++) {
						   for (Type t : typeList) {
							   if (t.getType().equals(type[i])) {
								   Type.add(t);
								   break;
							   }
						   }
					   }
					   teamMember.setPartyType(Type);
					   break;
					default:
						throw new IllegalArgumentException("invalid input: "+parts[0]);
				}
			}
			if (teamMember != null) {
				Team.add(teamMember);
			}
		}
		
		return Team;
	}
	
}
