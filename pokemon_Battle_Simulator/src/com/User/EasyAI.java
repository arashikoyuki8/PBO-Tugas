package com.User;

import com.Data.*;
import java.util.*;

public class EasyAI extends DifficultyAI{
	private Pokemon[] vAITeam;
	private Pokemon vCurrentPokemon;
	private Moves[] vAIMoves;
	private Player vPlayerTeam;
	private Moves choosenMove;
	private int actionToken;
	private Pokemon choosenPokemon;
	private double vDamageCount;
	private boolean isWin;
	private int isFainted;
	
	public EasyAI(List<Pokemon> pTeam, Player pPlayerTeam) {
		this.vAITeam = new Pokemon[pTeam.size()];
		for (int i = 0; i < pTeam.size(); i++) {
			this.vAITeam[i] = pTeam.get(i);
		}
		for (int i = 0; i < vAITeam.length; i++) {
			vAITeam[i].setMHP(vAITeam[i].getTHP());
			vAITeam[i].setRHP(vAITeam[i].getTHP());
		}
		this.vAIMoves = new Moves[4];
		this.vPlayerTeam = pPlayerTeam;
		this.isWin = true;
	}
	@Override
	public Pokemon getPokemon() {
		return vCurrentPokemon;
	}
	@Override
	public void mOpponentAction() {
		if (this.vCurrentPokemon.getRHP() != 0) {
			if (this.actionToken == 1) {
				this.mApplyDamage(choosenMove);
			} else {
				this.mSwitchPokemon(choosenPokemon);
			}
		} else {
			for (Pokemon p : vAITeam) {
				if (p.getRHP() == 0) {
					isFainted++;
				}
			}
			if (isFainted != vAITeam.length) {
				this.isFainted = 0;
				this.opponentCPokemon();
			} else {
				this.isWin = false;
			}
		}
	}
	@Override
	public Pokemon opponentCPokemon() {
	    int index = 0;
	    if (vAITeam[index].getRHP() == 0) {
	        index++;
	    }
	    this.vCurrentPokemon = vAITeam[index];
	    return vCurrentPokemon;
	}

	@Override
	public Moves opponentCMove(Pokemon p1) {
	    for (int i = 0; i < vCurrentPokemon.getPartyMoves().size(); i++) {
	        this.vAIMoves[i] = vCurrentPokemon.getPartyMoves().get(i);
	    }
	    for (Type t : vPlayerTeam.getPokemon().getPartyType()) {
	        for (Moves m : vAIMoves) {
	            if (t.getLessEffect().contains(m.getMoveType().getType()) || t.getNeutral().contains(m.getMoveType().getType())) {
	                this.choosenMove = m;
	                this.actionToken = 1;
	                return choosenMove;  // Exit the method after finding a move
	            }
	        }
	    }
	    // If no move is found, the method will reach here
	    this.actionToken = 1;
	    return null;  // Need to handle if no move found i guess
	}

	@Override
	public Pokemon opponentSwitch(Pokemon p1) {
		Random rd = new Random();
		int randomIndex = rd.nextInt(vAITeam.length);
		if (vAITeam[randomIndex].getRHP() == 0) {
			this.opponentSwitch(p1);
		} else {
			this.choosenPokemon = vAITeam[randomIndex];
		}
		this.actionToken = 0;
		return choosenPokemon;
	}

	@Override
	public void opponentDecision(Pokemon p1) {
		if (vCurrentPokemon.getRHP() == 0) {
			this.opponentSwitch(vPlayerTeam.getPokemon());
		} else {
			this.opponentCMove(vPlayerTeam.getPokemon());
		}
	}
	
	@Override
	public void mApplyDamage(Moves move) {
		if (move.IsSpecial()) {
			this.vDamageCount = (((2 * vCurrentPokemon.getLevel() / 5) + 2) * move.getMovePower() * vCurrentPokemon.getTSPA() / vPlayerTeam.getPokemon().getTSPD()) / 50 + 2;
		} else {
			this.vDamageCount = (((2 * vCurrentPokemon.getLevel() / 5) + 2) * move.getMovePower() * vCurrentPokemon.getTATK() / vPlayerTeam.getPokemon().getTDEF()) / 50 + 2;
		}
		if (vCurrentPokemon.getPartyType().contains(move.getMoveType())) {
			this.vDamageCount = vDamageCount * 1.5;
		}
		for (Type t :vPlayerTeam.getPokemon().getPartyType()) {
			if (t.getSuperEffect().contains(move.getMoveType().getType())) {
				this.vDamageCount = vDamageCount * 2;
			} else if (t.getLessEffect().contains(move.getMoveType().getType())) {
				this.vDamageCount = vDamageCount * 0.5;
			} else if (t.getNoEffect().contains(move.getMoveType().getType())) {
				this.vDamageCount = vDamageCount * 0;
			}
		}
		System.out.println(vCurrentPokemon.getPartyName() + " Use " + move.getMoves());
		System.out.println(vPlayerTeam.getPokemon().getPartyName() + " Take " + vDamageCount + " Damage!");
		if (vPlayerTeam.getPokemon().getRHP() <= vDamageCount) {
			vPlayerTeam.getPokemon().setRHP(0);
		} else {
			vPlayerTeam.getPokemon().setRHP(vPlayerTeam.getPokemon().getRHP() - vDamageCount);
		}
	}
	@Override
	public void mSwitchPokemon(Pokemon pokemon) {
		this.vCurrentPokemon = choosenPokemon;
	}
	@Override
	public boolean mIsWin() {
		return isWin;
	}
	
	//gui method
	
}
