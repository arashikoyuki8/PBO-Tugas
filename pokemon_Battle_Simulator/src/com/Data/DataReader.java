package com.Data;

import java.io.*;
import java.util.*;
import java.net.*;

public class DataReader {
	private List<Pokemon> vPlayerParty, vOpponentParty;
	
	public void setPlayerParty(List<Pokemon> team) {
		this.vPlayerParty = team;
	}
	public List<Pokemon> getPlayerParty(){
		return vPlayerParty;
	}
	
	public void setOpponentParty(List<Pokemon> team) {
		this.vOpponentParty = team;
	}
	public List<Pokemon> getOpponentParty(){
		return vOpponentParty;
	}
	
	public List<Pokemon> pokeReader() throws URISyntaxException {
		List<Pokemon> pokemonList = null;
		URL resource = getClass().getResource("/Pokemon.txt");
        if (resource == null) {
            // File not found
            return null;
        }
        File pokemon = new File(resource.toURI());
		try (BufferedReader br = new BufferedReader(new FileReader(pokemon.getAbsolutePath()))) {
			pokemonList = Pokemon.readPokemon(pokemon.getAbsolutePath());
			/*
			 * for (Pokemon p : pokemonList) {
			 * System.out.println(p);
			 * }
			 */
		} catch (IOException e) {
			e.printStackTrace();
		}
		return pokemonList;
	}	
	public List<Type> typeReader() throws URISyntaxException{
		List<Type> typeList = null;
		URL resource = getClass().getResource("/Type.txt");
        if (resource == null) {
            // File not found
            return null;
        }
        File type = new File(resource.toURI());
		try (BufferedReader br = new BufferedReader(new FileReader(type.getAbsolutePath()))) {
			  typeList = Type.readType(type.getAbsolutePath());
			  /*System.out.println("Type List:");
			  	for (TypeAdvantage m : typeList) {
			     System.out.println(m);
			  }*/
		} catch (IOException e) {
			   e.printStackTrace();
		}
		
		return typeList;
	}
	public List<Moves> moveReader() throws URISyntaxException{
		List<Moves> moveList = null;
		URL resource = getClass().getResource("/Moves.txt");
        if (resource == null) {
            // File not found
            return null;
        }
        File move = new File(resource.toURI());
		try (BufferedReader br = new BufferedReader(new FileReader(move.getAbsolutePath()))) {
			  moveList = Moves.readMoves(move.getAbsolutePath());
			  /*for (Moves m : moveList) {
			     System.out.println(m);
			  }*/
		} catch (IOException e) {
			   e.printStackTrace();
		}
		
		return moveList;
	}
	public List<Pokemon> partyReader() throws URISyntaxException{
		List<Pokemon> playersParty = null;
		URL resource = getClass().getResource("/PlayersParty1.txt");
        if (resource == null) {
            // File not found
            return null;
        }
        File playerParty = new File(resource.toURI());
		try (BufferedReader br = new BufferedReader(new FileReader(playerParty.getAbsolutePath()))){
			playersParty = Party.readParty(playerParty.getAbsolutePath());
			//for (Pokemon a : playersParty) {
			//	System.out.println(a);
			//}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return playersParty;
	}
	public List<Pokemon> opponentReader() throws URISyntaxException{
		List<Pokemon> opponentParty = null;
		URL resource = getClass().getResource("/AIParty1.txt");
        if (resource == null) {
            // File not found
            return null;
        }
        File AIParty = new File(resource.toURI());
		try (BufferedReader br = new BufferedReader(new FileReader(AIParty.getAbsolutePath()))){
			opponentParty = Party.readParty(AIParty.getAbsolutePath());
			//for (Pokemon a : playersParty) {
			//	System.out.println(a);
			//}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return opponentParty;
	}
	public List<Pokemon> CalcStat(List<Pokemon> list) throws URISyntaxException{
		List<Pokemon> dexList = pokeReader();
		List<Pokemon> Party = list;
		
		for (Pokemon a : Party) {
			for (Pokemon p : dexList) {
				if (a.getPartyName().equals(p.getDexName())) {
					a.setTHP(((p.getBHP()*2+a.getIVHP()+(a.getEVHP()/4))*a.getLevel())/100+a.getLevel()+10);
					a.setTATK(((p.getBATK()*2+a.getIVATK()+(a.getEVATK()/4))*a.getLevel())/100+5);
					a.setTDEF(((p.getBDEF()*2+a.getIVDEF()+(a.getEVDEF()/4))*a.getLevel())/100+5);
					a.setTSPA(((p.getBSPA()*2+a.getIVSPA()+(a.getEVSPA()/4))*a.getLevel())/100+5);
					a.setTSPD(((p.getBSPD()*2+a.getIVSPD()+(a.getEVSPD()/4))*a.getLevel())/100+5);
					a.setTSPE(((p.getBSPE()*2+a.getIVSPE()+(a.getEVSPE()/4))*a.getLevel())/100+5);
				}
			}
		}
		return Party;
	}
	
}
